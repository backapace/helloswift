//
//  ViewController.swift
//  HelloSwift
//
//  Created by 欧小强 on 2020/4/15.
//  Copyright © 2020 欧小强. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        /*
        let age = -10
        assert(age > 0, "A person age must not be less than zero")*/
        
        let study : basicStudy = basicStudy()
        study.textlog()
        
        var nameInstance = name()
        nameInstance.number = 10
        var nameFunc = nameInstance.myname()
        print(nameFunc)
        var shape = Squre(sideLength: 8, name: "Square")
        print(shape.simpleDescription())
        
        let triangle = EquilateralTriangle(sideLength: 3.1, name: "a triangle")
        print(triangle.simpleDescription())
        triangle.permeter = 9.9
        print(triangle.simpleDescription())
        
        let triangleAndSquare = TriangleAndSquare(size: 10, name: "another test shape")
        print(triangleAndSquare.square.sideLength)
        triangleAndSquare.square = Squre(sideLength: 50, name: "larger square")
        print(triangleAndSquare.triangle.sideLength)
        
        var counter = Counter()
        counter.incrementBy(amount: 2, numberOfTimes: 8)
        
        let ace = Rank.Ace
        let aceRawValue = ace.rawValue
        
        print(ace)
        print(aceRawValue)
        
        if let convertRank = Rank(rawValue: 3) {
            let description = convertRank.simpleDescription()
            print(description)
            
        }
        
        let hearts = Suit.Hearts
        let heartDescription = hearts.simpleDescription()
        print(heartDescription)
        
        let success = ServerResponse.Result("6 : 00 am", "8 : 00 pm")
        let failure = ServerResponse.Error("Out of cheese")
        
        checkRsp(res: success)
        checkRsp(res: failure)
        
        let threeOfSpades = Card(rank: .Three, suit: .Spades)
        let threeOfSpadesDescription = threeOfSpades.simpleDescription()
        print(threeOfSpadesDescription)
        
        let a = SimpleClass()
        a.adjust()
        let desc = a.simpleDescription
        print(desc)
        
        var b = SimpleStructure(simpleDescription: "A simple structure")
        b.adjust()
        print(b.simpleDescription)
        
        var c = 7;
        c.adjust();
        print(c.simpleDescription)
        
        var m = 10
        var n = 20
        swapTwoValue(a: &m, b: &n)
        print("a:\(m), b:\(n)")
        
        var possibleInteger : OptionalValue<Int> = .None
        possibleInteger = .Some(100)
        print(possibleInteger)
        
        let doubleValue = 000123.456
        let oneMillion  = 1_000_000
        let justOverOneMillion = 1_000_000.000_000_1
        print("\(doubleValue), \(oneMillion), \(justOverOneMillion)")
        
        typealias numberInt = UInt16
        print(numberInt.min)
        print(numberInt.max)
        
        let http404Error = (404, "Not Found")
        let (statusCode, statusMessage) = http404Error
        print("The status code is \(statusCode)")
        print("The status message is \(statusMessage)")
        
        #warning("貌似语法有变")
        /*
        let str = "123"
        let value = (str as NSString).integerValue
        if value != nil {
            print("\(str) has an integer value of \(value)")
        }
        else {
            print("\(str) couldn't be converted to an integer")
        }
        
        if let actualNumber = str.toValue() {
            print("\(str) has an integer value of \(actualNumber)")
        }
        else {
            print("\(str) couldn't be converter to an integer")
        }*/
        var serviceResponseCode : Int?=404
        serviceResponseCode = nil
        if serviceResponseCode==nil {
            print("")
        }
        
        let number1 = 9.9
        let number2 = 2.5
        let result = number1.truncatingRemainder(dividingBy: number2)
        print(result)
        
        let words = "\"我是齐天大圣\"-孙悟空"
        print("word = \(words)")
        
        let dollarSign = "\u{24}"
        print("dollarSign = \(dollarSign)")
        
        let blackHeart = "\u{2665})"
        print("blackHeart = \(blackHeart)")
        
        let sparklingHeart = "\u{1f496}"
        print("sparklingHeart = \(sparklingHeart)")
        
        var emptyStr = ""
        print("emptyStr = \(emptyStr)")
        
        var emptyStr1 = String()
        print("emptyStr1 = \(emptyStr1)")
        
        var varStr = "boy"
        varStr += " girl"
        print("varStr = \(varStr)")
        
        for character in "boys!" {
            print(character)
        }
        
        var str = "you are a good girl"
        print(str.count)
        
        var str1 = "hello"
        var str2 = "word"
        var strValue = str1+str2
        print("strV = \(strValue)")
        
        strValue = str1.appending(str2)
        print("strValue = \(strValue)")
        
        strValue = str1.appending("!")
        print("strValue = \(strValue)")
        
        let factor = 3
        let value = "\(factor) 乘以 2.5 是 \(Double(factor) * 2.5)"
        print("value = \(value)")
        
        str = "I am in Beijing City"
        if (str.hasPrefix("I")) {
            print("YES")
        } else {
            print("NO")
        }
        
        if (str.hasSuffix("City")) {
            print("YES")
        } else {
            print("NO")
        }
        
        str = "I am not a salted fish"
        str1 = str.uppercased()
        print(str1)
        str2 = str.lowercased()
        print(str2)
        
        for codeUnit in str.utf8 {
            print(codeUnit)
        }
    }
    
    func checkRsp(res:ServerResponse) {
        switch res {
        case let .Result(sunrise, sunset):
            let serverResponse = "Sunrise is at \(sunrise) and sunset is at \(sunset)"
            print(serverResponse)
        case let .Error(error):
            let serverResponse = "Failure...\(error)"
            print(serverResponse)
        }
    }
    
    func swapTwoValue<T>(a:inout T, b:inout T) {
        let tempValue = a
        a = b
        b = tempValue
    }
}

