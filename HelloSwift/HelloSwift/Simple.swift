//
//  Simple.swift
//  HelloSwift
//
//  Created by 欧小强 on 2020/4/15.
//  Copyright © 2020 欧小强. All rights reserved.
//

import Foundation
import UIKit

class basicStudy: NSObject {
    override init() {
        super.init()
        let greet = goodmorning(name: "John", location: "Beijing")
        print(greet)
        
        let scores = [100, 98, 78, 66, 54]
        let result = calculation(scores: scores)
        print("min：\(result.min)")
        print("max：\(result.max)")
        print("sum：\(result.sum)")
        
        let value = sum(numbers: 100, 80, 60, 70)
        print(value)
        
        let sum = count()
        print(sum)
        
        let result1 = increment()
        let value1 = result1(100)
        print(value1)
        
        let numbers = [20, 15, 18, 9]
        let matchCondition = match(list: numbers, condiction: lessThan)
        print(matchCondition)
    }
    
    func textlog () {
        let label = "The width is "
        let width = 100
        let widthLbl = label + String(width)
        print(widthLbl)
        
        let apples = 3
        let peaches = 5
        
        let appTotal = "I have \(apples) apples."
        print(appTotal)
        
        let fruitTotal = "I have \(apples+peaches) fruits"
        print(fruitTotal)
        
        var shoppingArr = ["bread", "beer", "sugar"]
        shoppingArr[0] = "water"
        print(shoppingArr)
        
        var dict = ["key1":"value1", "key2":"value2"]
        dict["key1"] = "value3"
        print(dict)
        
        var emptyArr = [String]()
        emptyArr = []
        var empytDict = [String:Float]()
        empytDict = [:]
        
        let studentScore = [100, 200, 120, 232]
        var aveScore = 0
        for score in studentScore {
            if score >= 200 {
                aveScore += 10
            }
            else {
                aveScore += 0
            }
        }
        print(aveScore)
        
        var optionalName : String? = "John Brown"
        var greet = "hello!"
        if let name = optionalName {
            greet = "Hello, \(name)"
        }
        print(greet)
        
        let vegetable = "red pepper"
        switch vegetable {
        case "celery":
            let comment = "celery"
            print(comment)
        case "cucumber", "watercress":
            let comment = "cucumber, watercress"
            print(comment)
        case let x where x.hasSuffix("pepper"):
            let comment = "It is a \(x)"
            print(comment)
            
        default:
            let comment = "nothing"
            print(comment)
            
        }
        
        let numberDict = ["Prime" : [2, 3, 5, 7, 11, 13], "Fibonacci" : [1, 1, 2, 3, 5, 8], "Square" : [1, 4, 9, 16, 25],]
        var largestNumber = 0
        for (_, numbers) in numberDict {
            for number in numbers {
                if number>largestNumber {
                    largestNumber = number
                }
            }
        }
        print(largestNumber)
        
        var n = 2
        while n<=200 {
            n = n*2
        }
        print(n)
        
        var value = 0
        for i in 0 ... 4 {
            value += i;
        }
        print("value = \(value)")
        
        var key = 0
        for j in 0 ..< 4 {
            key += j
        }
        print("key = \(key)")
        
    }

    func goodmorning(name:String, location:String) -> String {
        return "goodmorning!\(name), I am in \(location)"
    }
    
    func calculation(scores:[Int]) -> (min:Int, max:Int, sum:Int) {
        var min = scores[0]
        var max = scores[0]
        var sum = 0
        for score in scores {
            if score > max {
                max = score;
            }
            else if score < min {
                min = score
            }
            sum += score
        }
        return (min, max, sum)
        
        
    }
    
    func sum(numbers:Int ...) -> Int {
        var sum = 0
        for number in numbers {
            sum += number
        }
        return sum
        
    }
    
    func count() -> Int {
        var y = 100
        func add() {
            y += 10;
        }
        add()
        return y
    }
    
    func increment() -> ((Int) -> Int) {
        func add(numner:Int) -> Int {
            return 100+numner
        }
        return add
    }
    
    func match(list:[Int], condiction:(Int)->Bool) -> Bool {
        for item in list {
            if condiction(item) {
                return true
            }
        }
        return false
    }
    
    func lessThan(number:Int) -> Bool {
        return number<18
    }
    
    
}

class name {
    var number = 0

    func myname() -> String {
        return "John"
    }
}


class NamedShape {
    var numberOfSliders : Int = 0
    var name : String
    
    init(name:String) {
        self.name = name
    }
    
    func simpleDescription() -> String {
        return "A shape with \(numberOfSliders) sides"
    }
    
    deinit {
        print("NamedShape deinit")
    }
}

class Squre: NamedShape {
    var sideLength : Double = 0.0
    
    init(sideLength : Double, name : String) {
        self.sideLength = sideLength
        super.init(name: name)
    }
    
    override func simpleDescription() -> String {
        return "子类在重写父类方法"
    }
    
    deinit {
        print("Squre deinit")
    }
}

class EquilateralTriangle: NamedShape {
    var sideLength : Double = 0.0
    
    init(sideLength : Double, name : String) {
        self.sideLength = sideLength
        super.init(name: name)
        numberOfSliders = 3
    }
    
    var permeter : Double {
        get {
            return 3.0 * sideLength
        }
        set {
            sideLength = newValue / 3.0
        }
    }
    
    override func simpleDescription() -> String {
        return "An equilateral Triangle with sides of length \(sideLength)"
    }
    
    deinit {
        print("EquilateralTriangle deinit")
    }
}

class TriangleAndSquare {
    var triangle : EquilateralTriangle {
        willSet {
            square.sideLength = newValue.sideLength
        }
    }
    
    var square : Squre {
        willSet {
            triangle.sideLength = newValue.sideLength
        }
    }
    
    init(size:Double, name:String) {
        square = Squre(sideLength: size, name: name)
        triangle = EquilateralTriangle(sideLength: size, name: name)
    }
}


class JJSwiftVC: UIViewController {
    
}

class Counter {
    var count : Int = 0
    func incrementBy(amount : Int, numberOfTimes times : Int) {
        count += amount * times
        print(count)
    }
    
}


enum Rank : Int {
    case Ace = 1
    case Two, Three, Four, Five
    case Jack, Lucy, Queen
    func simpleDescription() -> String {
        switch self {
        case .Ace:
            return "Ace"
        case .Jack:
            return "Jack"
        case .Queen:
            return "Queen"
        default:
            return String(self.rawValue)
        }
    }
}

enum Suit {
    case Spades, Hearts, Diamonds, Clubs
    func simpleDescription() -> String {
        switch self {
        case .Spades:
            return "spades"
        case .Hearts:
            return "hearts"
        case .Diamonds:
            return "diamonds"
        case .Clubs:
            return "clubs"
        }
    }
}

enum ServerResponse {
    case Result(String, String)
    case Error(String)
}

struct Card {
    var rank : Rank
    var suit : Suit
    func simpleDescription() -> String {
        return "The \(rank.simpleDescription()) of \(suit.simpleDescription())"
    }
}


protocol ExampleProtocol {
    var simpleDescription : String { get }
    mutating func adjust()
}

class SimpleClass : ExampleProtocol {
    var simpleDescription: String = "It is a simple class"
    var anotherProperty : Int = 733800
    
    func adjust() {
        simpleDescription += "Now 100% adjusted"
    }
}


struct SimpleStructure : ExampleProtocol {
    var simpleDescription: String
    mutating func adjust() {
         simpleDescription += " (adjust)"
    }
    
}

extension Int : ExampleProtocol {
    var simpleDescription: String {
        return "The number is \(self)"
    }
    mutating func adjust() {
        self += 42
    }
}

enum OptionalValue <T> {
    case None
    case Some(T)
}
